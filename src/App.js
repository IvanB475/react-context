import './App.css';
import { Box } from './theme';
import React from 'react';

function App() {
  const [ font, setFont ] = React.useState("normal");
  
  const toggleFont = () => {
    const newFont = font === 'normal' ? 'big' : 'normal';
    setFont(newFont);
  }

  return (
    <div className="App">
      <Box theme='dark' onClick={toggleFont} font={font}></Box>
    </div>
  );
};



export default App;
