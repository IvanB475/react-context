import { Component, createContext } from "react";

export const ThemeContext = createContext(undefined);

export class ThemeProvider extends Component {
    state = {
        theme: "light",
        font: "normal",
        toggleTheme: this.setThemeMode,
        toggleFont: this.setThemeFont
    }
    setThemeMode = () => {
        const newTheme = this.state.theme === 'light' ? 'dark' : 'light';
        this.setState(...this.state, {theme: newTheme})
    };

    setThemeFont = () => {
        const newFont = this.state.font === 'normal' ? 'big' : 'normal';
        this.setState(...this.state, {font: newFont})
    };

    render() {
        return (
            <ThemeContext.Provider value={this.state}>{this.props.children}</ThemeContext.Provider>
        )
    }
}