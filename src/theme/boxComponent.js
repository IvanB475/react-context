import React, { Component, useContext } from "react";
import { ThemeContext, ThemeProvider } from "./themeContext";
import '../styles.css';

function ThemeBox({theme, font, toggleTheme, toggleFont}) {
    const themeClass = theme;
    const fontClass = font;
    console.log(themeClass);
    return (
        <div>
            <h1>Title</h1>
                <ThemeProvider>
                <ThemeContext.Consumer>
                    {({toggleTheme}) => (
                    <button onClick={toggleTheme}>
                    Toggle theme
                    </button>
                     )}
{/*                     <div className ={themeClass} >
                    <p className={fontClass}>some text inside the box</p>
                    </div> */}
                </ThemeContext.Consumer>
                <ThemeContext.Consumer>
                {({toggleFont}) => (
                    <button onClick={toggleFont}>
                    Toggle font
                    </button>
                     )}
                </ThemeContext.Consumer>
                <ThemeContext.Consumer>
                    {({}) => (
                        <div className ={themeClass} >
                        <p className={fontClass}>some text inside the box</p>
                        </div> 
                    )}
                </ThemeContext.Consumer>
                </ThemeProvider>
        </div>
    )
}

export default class Box extends Component {
    render() {
        const { theme, font } = this.props;
        return (
            <div>
            <ThemeBox {...{theme, font}}></ThemeBox>
            </div>
        )
    }
}